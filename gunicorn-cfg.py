# -*- encoding: utf-8 -*-

bind = '0.0.0.0:5005'
threads = 2
# for logging to stdout
accesslog = '-'
loglevel = 'error' # 'debug'
capture_output = True
enable_stdio_inheritance = True

# gevent setup
workers = 2
worker_class = 'gevent'
# The maximum number of simultaneous clients.
# This setting only affects the Eventlet and Gevent worker types.
worker_connections = 4
