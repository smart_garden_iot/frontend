import pandas as pd
from app.influxdb import get_db
import ciso8601


def get_measurements(bucket):

    query_api = get_db().query_api()

    p = {"_bucket": bucket}

    q = """
        import "influxdata/influxdb/schema"
        schema.measurements(bucket: _bucket)
    """

    measures_list = list(query_api.query_data_frame(
        query=q, params=p)["_value"])
    measures_list.remove("state")
    measures_list.remove("wateringState")
    return [({"label": i, "value": i}) for i in measures_list]


def get_instant_from_measure(bucket, measure):

    p = {"_bucket": bucket,
         "_measure": measure
         }

    q = f"""
    from(bucket: _bucket)
        |> range(start: -3s)
        |> filter(fn: (r) => r._measurement == _measure)
        |> filter(fn: (r) => r._field == "value")
        |> last()
    """
    sensor_readings = get_db(). \
        query_api(). \
        query_data_frame(query=q, params=p)

    if sensor_readings.empty:
        q = f"""
            from(bucket: _bucket)
                |> range(start: -7d)
                |> filter(fn: (r) => r._measurement == _measure)
                |> filter(fn: (r) => r._field == "value")
                |> last()
            """
        sensor_readings = get_db(). \
            query_api(). \
            query_data_frame(query=q, params=p)

    if sensor_readings.empty:
        return None
    else:
        return sensor_readings


def get_time_series_df_from_measure(bucket, measure):

    p = {"_bucket": bucket,
         "_measure": measure
         }

    q = f"""
    from(bucket: _bucket)
        |> range(start: -7d)
        |> filter(fn: (r) => r._measurement == _measure)
        |> filter(fn: (r) => r._field == "value")
    """
    sensor_readings = get_db(). \
        query_api(). \
        query_data_frame(query=q, params=p)

    return sensor_readings


def get_heatmap_data(bucket, measure, year):
    t = str(year) + "-01-01T00:00:00Z"

    p = {"_bucket": bucket,
         "_timestamp": ciso8601.parse_datetime(t),
         "_measure": measure
         }

    q = """
        from(bucket: _bucket)
            |> range(start: _timestamp)
            |> filter(fn: (r) => r._measurement == _measure)
            |> filter(fn: (r) => r._field == "value")
            |> aggregateWindow(every: 1d, fn: mean)
        """

    sensor_readings = get_db(). \
        query_api(). \
        query_data_frame(query=q, params=p)

    return sensor_readings


def process_heatmap_data(heatmap_df):
    heatmap_df["_time"] = pd.to_datetime(
        heatmap_df["_time"],
        format="%Y%m%d%H%M%S%z")

    heatmap_df['day_of_year'] = heatmap_df["_time"].dt.dayofyear

    heatmap_df = heatmap_df.fillna(0)
    heatmap_df.sort_values(['day_of_year'], ascending=True, inplace=True)
    heatmap_df[heatmap_df["_value"] == 0.0] = -1.0

    return heatmap_df["_value"]
