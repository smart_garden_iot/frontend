import os
from flask import g
import paho.mqtt.client as mqtt


def get_conn(onconnect=None):
    """
    Connect to the application's configured MQTT broker. The connection
    is unique for each request and will be reused if this is called
    again.
    """
    mqttBroker = os.getenv("MQTT_BROKER")
    if "mqtt_conn" not in g:
        g.mqtt_client = mqtt.Client("Flask_server_client")
        g.mqtt_client.on_connect=onconnect
        g.mqtt_client.connect(mqttBroker)
    return g.mqtt_client


def close_conn(e=None):
    """
    If this request connected to the broker, close the connection.
    """
    mqtt_client = g.pop("mqtt_conn", None)

    if mqtt_client is not None:
        g.mqtt_client.disconnect()

    return None


def init_app(app):
    """
    Register mqtt functions with the Flask app. This is called by
    the application factory.
    """
    app.teardown_appcontext(close_conn)
