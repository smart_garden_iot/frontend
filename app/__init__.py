from flask import Flask, render_template
import os
import logging
from app.mqtt import get_conn


def create_app():
    """Factory function that creates the Flask app"""

    app = Flask(__name__)

    app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")
    app.config["MAX_CONTENT_LENGTH"] = 2 * 1024 * 1024
    app.config["ALLOWED_EXTENSIONS"] = [".yaml", ".yml"]
    app.config["UPLOAD_PATH"] = "resources"

    logging.basicConfig(level=logging.DEBUG)

    @app.route("/")
    def home():
        return render_template("index.html")

    from app import influxdb, mqtt
    from app.ruleset import upload_rules
    from app.dash_setup import register_dash_app
    from app.ruleset.utils import on_connect, update_soil_humidity

    influxdb.init_app(app)
    mqtt.init_app(app)
    register_dash_app(app)
    app.register_blueprint(upload_rules.bp, url_prefix="/rules")

    with app.app_context():
        path = os.path.join(
            app.root_path,
            app.config["UPLOAD_PATH"],
            "rules.yaml")

        mqtt_client = get_conn(on_connect)
        update_soil_humidity(path, mqtt_client)

    return app
