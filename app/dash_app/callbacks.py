from dash.dependencies import Input, Output, State
import datetime
from dash.exceptions import PreventUpdate
from app import graphs, query
import plotly.graph_objs as go
import dash_html_components as html
import dash_bootstrap_components as dbc

int_to_state = ["Idle",
                "Poll",
                "Ready",
                "Watering",
                "Monitor",
                "Alarm",
                "Connection error"]

int_to_watering_state = ["Automatic", "Manual", "Not watering"]


def register_callbacks(dash_app, bucket):
    @dash_app.callback(
        Output("instant-text", "children"),
        Input("interval-component", "n_intervals"),
        State("measurements_dropdown", "value")
    )
    def update_metric(n, measurements_dropdown_value):
        if measurements_dropdown_value is None:
            return None

        measure = query.get_instant_from_measure(
            bucket,
            measurements_dropdown_value,
        )

        if measure is None:
            value = 0
        else:
            value = measure["_value"][0]

        style = {'padding': '5px',
                 'fontSize': '16px',
                 "display": "table",
                 "margin": "0 auto"}
        return [
            html.Span(f"{measurements_dropdown_value} : {value}", style=style),
        ]

    @dash_app.callback(
        Output("static-line-plot", "children"),
        Input("measurements_dropdown", "value")
    )
    def plot_static_time_series(measurements_dropdown_value):

        if measurements_dropdown_value is None:
            return None

        time_series_df = query.get_time_series_df_from_measure(
            bucket,
            measurements_dropdown_value
        )

        trace = go.Scatter(
            x=time_series_df["_time"],
            y=time_series_df["_value"],
            name=measurements_dropdown_value
        )

        line_plot = graphs.get_static_line_plot(
            trace,
            f"Time series of {measurements_dropdown_value}"
        )

        return line_plot

    @dash_app.callback(
        Output("heatmap_col", "children"),
        Input("measurements_dropdown", "value")
    )
    def plot_calendar_heatmap(measurements_dropdown_value):

        if measurements_dropdown_value is None:
            return None

        heatmap_df = query.get_heatmap_data(
            bucket,
            measurements_dropdown_value,
            datetime.datetime.now().year
        )

        heatmap = graphs.get_calendar_heatmap(
            query.process_heatmap_data(heatmap_df),
            f"Monthly {measurements_dropdown_value}")

        return heatmap

    @dash_app.callback(
        Output("session", "system_state_data"),
        Input("system-state-text", "children"),
        State("session", "system_state_data")
    )
    def update_system_state(children, state_data):
        if children is None:
            raise PreventUpdate

        measure = query.get_instant_from_measure(
            bucket,
            "state",
        )
        if measure is not None:
            state_data = \
                state_data or {"system_state": int(measure["_value"][0])}
            state_data["system_state"] = int(measure["_value"][0])
        return state_data

    @dash_app.callback(
        Output("system-state-text", "children"),
        Input("interval-component", "n_intervals"),
        State("session", "system_state_data")
    )
    def show_system_state(n, state_data):
        if state_data is not None:
            value = state_data["system_state"]
        else:
            measure = query.get_instant_from_measure(
                bucket,
                "state",
            )
            value = int(measure["_value"][0]) or 4

        card_content = [
            dbc.CardHeader("System State"),
            dbc.CardBody(
                [
                    html.H5(int_to_state[value], className="card-title"),
                ]
            ),
        ]
        return dbc.Card(card_content, color="light")

    @dash_app.callback(
        Output("session", "state_data"),
        Input("watering-state-text", "children"),
        State("session", "state_data")
    )
    def update_watering_state(children, state_data):
        if children is None:
            raise PreventUpdate

        measure = query.get_instant_from_measure(
            bucket,
            "wateringState",
        )
        if measure is not None:
            state_data = \
                state_data or {"watering_state": int(measure["_value"][0])}
            state_data["watering_state"] = int(measure["_value"][0])
        return state_data

    @dash_app.callback(
        Output("watering-state-text", "children"),
        Input("interval-component", "n_intervals"),
        State("session", "state_data")
    )
    def show_watering_state(n, state_data):
        if state_data is not None:
            value = state_data["watering_state"]
        else:
            measure = query.get_instant_from_measure(
                bucket,
                "wateringState",
            )
            value = int(measure["_value"][0]) or 2

        card_content = [
            dbc.CardHeader("Watering State"),
            dbc.CardBody(
                [
                    html.H5(int_to_watering_state[value],
                            className="card-title"),
                ]
            ),
        ]
        return dbc.Card(card_content, color="light")
