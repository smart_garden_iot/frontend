import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
# from flask_dashboard import graphs
import app.query as query


def get_navbar():
    return dbc.NavbarSimple(
        brand="Smart Garden",
        brand_href="#",
        color="dark",
        dark=True
    )


def get_body():

    return dbc.Row(
        [
            dbc.Col(
                [
                    html.Label(
                        'Measurements',
                        style={'margin-top': '1.5em'}),
                    dcc.Dropdown(
                        options=query.get_measurements("smart_garden"),
                        value=None,
                        id="measurements_dropdown"
                    )
                ], xs=12, sm=6, md=4
            )
        ]
    )


def get_instant_text_row():
    return dbc.Row(
        dbc.Col(
            id="instant-text"
        )
    )


def get_static_line_plot_row():
    return dbc.Row(
        dbc.Col(
            id="static-line-plot"
        )
    )


def get_heatmap_row():
    return dbc.Row(
        dbc.Col(
            id="heatmap_col"
        )
    )


def get_update_interval():
    return dcc.Interval(
        id='interval-component',
        interval=1*1000,  # in milliseconds
        n_intervals=0
    )


def get_watering_state_row():
    return dbc.Row(
        dbc.Col(
            id="watering-state-text"
        )
    )


def get_system_state():
    return dbc.Row(
        dbc.Col(
            id="system-state-text"
        )
    )


def get_base_layout():
    return html.Div([
        dcc.Location(id='url', refresh=False),
        html.Div(id='page-content')
    ])


def get_layout_system_state_page():
    return dbc.Container(
        [
            get_navbar(),
            get_body(),
            get_instant_text_row(),
            get_static_line_plot_row(),
            get_heatmap_row(),
            get_update_interval()
        ],
    )


def get_layout_stats_page():
    return dbc.Container(
        [
            dcc.Store(id='session',
                      storage_type='session',
                      data={"watering_state": 0,
                            "system_state": 0}),
            get_navbar(),
            get_system_state(),
            get_watering_state_row(),
            get_update_interval()
        ],
    )
