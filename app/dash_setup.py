import dash
from dash.dependencies import Input, Output


def register_dash_app(app):
    """
    Register Dash app with the Flask app
    """

    external_stylesheets = [
        'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min'
        '.css '
    ]

    external_scripts = [
        "https://code.jquery.com/jquery-3.5.1.slim.min.js",
        "https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js",
        "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js",
    ]

    # Add responsive viewport meta tag
    meta_viewport = [{
        "name": "viewport",
        "content": "width=device-width, initial-scale=1, shrink-to-fit=no"
    }]

    dash_app = dash.Dash(
        __name__,
        server=app,
        url_base_pathname='/dash/',
        meta_tags=meta_viewport,
        external_scripts=external_scripts,
        external_stylesheets=external_stylesheets,
        update_title=None,
        suppress_callback_exceptions=True
    )
    dash_app.title = 'Smart Garden Dashboard'

    # Imports inside the function to avoid circular dependencies
    from app.dash_app.layout import get_layout_stats_page, \
        get_layout_system_state_page, get_base_layout
    from app.dash_app.callbacks import register_callbacks

    with app.app_context():

        # Assign the get_layout function without calling it yet
        dash_app.layout = get_base_layout()

        # Register callbacks
        # Layout must be assigned above, before callbacks

        @dash_app.callback(Output('page-content', 'children'),
                           Input('url', 'pathname'))
        def display_page(pathname):
            if pathname == '/dash/readingsOverview':
                return get_layout_system_state_page()
            elif pathname == '/dash/stateOverview':
                return get_layout_stats_page()
            else:
                return '404'

        register_callbacks(dash_app, "smart_garden")

    return None
