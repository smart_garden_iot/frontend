from flask import g
import influxdb_client


def get_db():
    """
    Connect to the application's configured database. The connection
    is unique for each request and will be reused if this is called
    again.

    To use query_api = client.query_api()
    """
    if "db" not in g:
        g.db = influxdb_client.InfluxDBClient.from_env_properties()
    return g.db


def close_db(e=None):
    """If this request connected to the database, close the connection.
    """
    db = g.pop("db", None)

    if db is not None:
        db.close()

    return None


def init_app(app):
    """Register database functions with the Flask app. This is called by
    the application factory.
    """
    app.teardown_appcontext(close_db)
