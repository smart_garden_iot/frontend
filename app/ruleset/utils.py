import json
import yaml
from flask import current_app, redirect, url_for
from schema import Schema, Or, SchemaError


def update_soil_humidity(path, mqtt_client):
    cfg, jsonDoc = validate_ruleset(path)
    max_soil_humidity = None
    min_soil_humidity = None

    for c in cfg["rules"]:
        for cc in c["conditions"]:
            if cc["type"] == "upperBound":
                max_soil_humidity = cc["value"]
            elif cc["type"] == "lowerBound":
                min_soil_humidity = cc["value"]

    if max_soil_humidity is not None and min_soil_humidity is not None:
        mqtt_client.publish(
            "system/soilHumidityThreshold",
            json.dumps(
                {"min": min_soil_humidity, "max": max_soil_humidity}))


def validate_ruleset(path):
    # file parsing
    with open(path, "r") as f:
        try:
            cfg = yaml.safe_load(f)
            jsonDoc = json.dumps(cfg)
        except yaml.YAMLError as err_msg:
            current_app.logger.info(
                "Failed to parse file given with error: %s", err_msg)
            return redirect(url_for("rules.form"))

    current_app.logger.info(cfg)
    current_app.logger.info(jsonDoc)

    # file validation
    config_schema = Schema({
        "rules": {
            "measure": Or("soilHumidity",
                          "tankLevel",
                          "temperature",
                          "airHumidity",
                          "pressure"),
            "conditions": [{
                "type": Or("upperBound", "lowerBound"),
                "value": int
            }]
        }
    })

    # validate each chunk
    for c in cfg["rules"]:
        c = {"rules": c}
        try:
            config_schema.validate(c)
        except SchemaError as se:
            current_app.logger.info("Error %s", se)
            return redirect(url_for("rules.form"))

    return cfg, jsonDoc


# update thresholds accordingly through MQTT
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print("Failed to connect, return code %d\n", rc)
