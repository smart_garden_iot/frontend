import os
from os.path import exists
from flask import render_template, request, url_for, send_from_directory
from flask import Blueprint, current_app
from app.ruleset.utils import update_soil_humidity
from werkzeug.utils import secure_filename, redirect
from app.mqtt import get_conn

bp = Blueprint("rules", __name__, template_folder="templates")


def get_rules_path():
    return os.path.join(
        current_app.root_path,
        current_app.config["UPLOAD_PATH"],
        "rules.yaml")


@bp.route("/browse")
def browse_rules():
    path = get_rules_path()
    if not exists(path):
        return render_template("404.html")

    with open(path, "rb") as f:
        f = f.read().decode("utf-8")
        return render_template("browse_rules.html", file=f)


@bp.route("/download/<filename>")
def download_rules(filename):
    print(filename)
    return send_from_directory(
        current_app.config['UPLOAD_PATH'], filename)


@bp.route("/reset")
def reset_rules():
    path = get_rules_path()

    if exists(path):
        os.remove(path)
    return redirect("/")


# def allowed_file(filename):
#     return "." in filename and \
#            filename.rsplit(".", 1)[1].lower() in \
#            current_app.config["ALLOWED_EXTENSIONS"]


@bp.route('/')
def form():
    return render_template("request_rules_upload.html")


@bp.route("/upload", methods=["POST"])
def upload_rules():
    if "data_file" not in request.files:
        current_app.logger.info("No file part")
        return redirect(url_for("rules.form"))

    file = request.files["data_file"]

    if file.filename == "":
        current_app.logger.info("No selected file")
        return redirect(url_for("rules.form"))

    if not file:
        current_app.logger.info("Something's gone wrong")
        return redirect(url_for("rules.form"))

    filename = secure_filename(file.filename)

    if filename.lower().endswith(".yaml"):
        current_app.logger.info('File successfully uploaded')
    else:
        current_app.logger.info("File type not allowed")
        return redirect(url_for("rules.form"))

    path = os.path.join(
        current_app.root_path,
        current_app.config["UPLOAD_PATH"],
        filename)
    if exists(path):
        os.remove(path)
    file.save(path)

    update_soil_humidity(path, mqtt_client=get_conn())

    current_app.logger.info(path)

    new_path = get_rules_path()
    os.rename(path, new_path)

    return redirect(url_for("rules.browse_rules"))
