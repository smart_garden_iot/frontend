import numpy as np
import dash_core_components as dcc
import calendar
import plotly.graph_objs as go
import datetime


def get_calendar_heatmap(z,
                         title,
                         year: int = None,
                         month_lines: bool = True,
                         fig=None,
                         row: int = None):
    if year is None:
        year = datetime.datetime.now().year

    isleap = calendar.isleap(year)

    if isleap is True:
        data = np.ones(366) * np.nan
    else:
        data = np.ones(365) * np.nan

    data[:len(z)] = z

    d1 = datetime.date(year, 1, 1)
    d2 = datetime.date(year, 12, 31)

    delta = d2 - d1

    month_names = \
        ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct',
         'Nov', 'Dec']
    month_days = \
        [31, 28, 31, 30, 31, 30, 31, 31, 30, 31,
         30, 31]
    month_positions = (np.cumsum(month_days) - 15) / 7

    dates_in_year = [d1 + datetime.timedelta(i) for i in range(
        delta.days + 1)]  # gives a list with datetimes for each day a year
    weekdays_in_year = [i.weekday() for i in
                        dates_in_year]
    # gives [0,1,2,3,4,5,6,0,1,2,3,4,5,6,…] (ticktext in xaxis dict translates
    # this to weekdays

    weeknumber_of_dates = \
        [int(i.strftime("%V")) if not (
            int(i.strftime("%V")) == 1 and i.month == 12) else 53
         for i in dates_in_year]  # gives [1,1,1,1,1,1,1,2,2,2,2,2,2,2,…]
    text = [str(f'{e:9.2f}') for e in z]
    # gives ‘2018-01-25’ as hovertext.
    colorscale = [[False, '#eeeeee'], [True, '#76cf63']]

    data = [
        go.Heatmap(
            x=weeknumber_of_dates,
            y=weekdays_in_year,
            z=data,
            text=text,
            hoverinfo='text',
            xgap=3,
            ygap=3,
            showscale=False,
            colorscale=colorscale
        )
    ]

    if month_lines:
        kwargs = dict(
            mode='lines',
            line=dict(
                color='#9e9e9e',
                width=1
            ),
            hoverinfo='skip'

        )
        for date, dow, wkn in zip(dates_in_year,
                                  weekdays_in_year,
                                  weeknumber_of_dates):
            if date.day == 1:
                data += [
                    go.Scatter(
                        x=[wkn - .5, wkn - .5],
                        y=[dow - .5, 6.5],
                        **kwargs
                    )
                ]
                if dow:
                    data += [
                        go.Scatter(
                            x=[wkn - .5, wkn + .5],
                            y=[dow - .5, dow - .5],
                            **kwargs
                        ),
                        go.Scatter(
                            x=[wkn + .5, wkn + .5],
                            y=[dow - .5, -.5],
                            **kwargs
                        )
                    ]

    layout = go.Layout(
        title=title,
        height=250,
        yaxis=dict(
            showline=False, showgrid=False, zeroline=False,
            tickmode='array',
            ticktext=['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            tickvals=[0, 1, 2, 3, 4, 5, 6],
            autorange="reversed"
        ),
        xaxis=dict(
            showline=False, showgrid=False, zeroline=False,
            tickmode='array',
            ticktext=month_names,
            tickvals=month_positions
        ),
        plot_bgcolor='#fff',
        margin=dict(t=40),
        showlegend=False
    )

    if fig is None:
        fig = go.Figure(data=data, layout=layout)
    else:
        fig.add_traces(data, rows=[(row + 1)] * len(data), cols=[1] * len(data))
        fig.update_layout(layout)
        fig.update_xaxes(layout['xaxis'])
        fig.update_yaxes(layout['yaxis'])

    return dcc.Graph(id="live-update-heatmap",
                     config=dict(
                         displayModeBar=False
                     ),
                     figure=fig)


def get_static_line_plot(trace, title):
    return dcc.Graph(
        # Disable the ModeBar with the Plotly logo and other buttons
        config=dict(
            displayModeBar=False
        ),
        figure=go.Figure(
            data=[trace],
            layout=go.Layout(
                title=title,
                plot_bgcolor="white",
                xaxis=dict(
                    autorange=True,
                    # Time-filtering buttons above chart
                    rangeselector=dict(
                        buttons=list([
                            dict(count=1,
                                 label="1d",
                                 step="day",
                                 stepmode="backward"),
                            dict(count=7,
                                 label="7d",
                                 step="day",
                                 stepmode="backward"),
                            dict(count=1,
                                 label="1m",
                                 step="month",
                                 stepmode="backward"),
                            dict(step="all")
                        ])
                    ),
                    type="date",
                    # Alternative time filter slider
                    rangeslider=dict(
                        visible=True
                    )
                )
            )
        )
    )


def get_water_amount_plot(trace, title):
    return dcc.Graph(
        id="live-update-graph",
        figure=go.Figure(
            layout=go.Layout(
                xaxis=dict(
                    visible=False,
                    type="date"
                ),
                yaxis=dict(
                    visible=False
                ),
                annotations=[dict(
                    text="No matching data found",
                    xref="paper",
                    yref="paper",
                    showarrow=False,
                    font=dict(
                        size=28
                    )
                )]
            )
        )
    )
